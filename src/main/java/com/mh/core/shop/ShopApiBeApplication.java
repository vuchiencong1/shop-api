package com.mh.core.shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopApiBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShopApiBeApplication.class, args);
    }

}
